#ifndef REGISTRATION_H
#define REGISTRATION_H

#include <QDialog>
#include "login.h"
namespace Ui {
class registration;
}

class registration : public QDialog
{
    Q_OBJECT

public:
    explicit registration(QWidget *parent = 0, Storage *storage=nullptr);
    ~registration();
    int newid;

private slots:
    void on_oklog_clicked();

    void on_nolog_clicked();

private:
    Ui::registration *ui;
    Storage *storage_;
};

#endif // REGISTRATION_H
