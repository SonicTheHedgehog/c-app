#include "newgroup.h"
#include "ui_newgroup.h"

newgroup::newgroup(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::newgroup)
{
    ui->setupUi(this);
}

newgroup::~newgroup()
{
    delete ui;
}
Groups newgroup::MainSMedia()
{
    Groups group;
    group.name= ui->lineEdit->text().toStdString();
    group.users= ui->spinBox->value();
    group.admin= ui->lineEdit2->text().toStdString();
    return group;
}

void newgroup::EditLabels(const Groups &group)
{
    ui->lineEdit->setText(QString::fromStdString( group.name));
    ui->spinBox->setValue(group.users);
    ui->lineEdit2->setText(QString::fromStdString( group.admin));
}
