#include "registration.h"
#include "ui_registration.h"

registration::registration(QWidget *parent, Storage *storage) :
    QDialog(parent),
    ui(new Ui::registration),
    storage_(storage)
{
    ui->setupUi(this);
}

registration::~registration()
{
    delete ui;
}
void registration::on_oklog_clicked()
{
    try
    {
        Users u;
        u.username = ui->login2->text().toStdString();
        string password = ui->password->text().toStdString();
        string password2= ui->password2->text().toStdString();
        u.fullname=ui->fname->text().toStdString();
        login log;
        if(u.username==""|| password==""||password2==""|| u.fullname=="")
        {
            QMessageBox::information(this, "Failed", "Fill in all fields");
        }
        else if(password!=password2)
        {
            QMessageBox::information(this, "Failed", "Enter password again");
        }
        else
        {
            u.password_hash = log.hashPassword(QString::fromStdString(password)).toStdString();
            bool v=storage_->registr(u);
            if(v)
            {
                newid=storage_->insertUser(u);
                if(newid!=0)
                {
                    QMessageBox::information(this, "Success", "User was added");
                    this->done(1);
                    this->close();
                }
                else
                {
                    QMessageBox::information(this,"Error", "Cannot add this user");
                }
            }
            else
            {
                QMessageBox::information(this, "Failed", "User with this login is already exist.");
            }
        }
    }
    catch(...)
    {
        QMessageBox::warning(this, " Error","Server hasn't been opened.");
        exit(1);
    }

}

void registration::on_nolog_clicked()
{
    try
    {
        this->done(0);
        this->close();
    }
    catch(...)
    {
        QMessageBox::warning(this, " Error","Server hasn't been opened.");
        exit(1);
    }
}
