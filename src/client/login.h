#ifndef LOGIN_H
#define LOGIN_H
#include "remote_storage.h"
#include "registration.h"
#include <QAction>
#include <QMessageBox>
#include <QDialog>
#include <QCryptographicHash>
#include <experimental/optional>
namespace Ui {
class login;
}

class login : public QDialog
{
    Q_OBJECT

public:
    explicit login(QWidget *parent = 0, Storage *storage=nullptr);
    ~login();
    Users getUser();
    QString hashPassword(QString const & password);

private slots:
    void on_oklog_clicked();

    void on_nolog_clicked();

    void on_reg_clicked();

private:
    Ui::login *ui;
    Storage *storage_;
     Users cur_user;


};

#endif // LOGIN_H
