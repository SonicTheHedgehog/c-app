#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    info(false);
    info2(false);

    setWindowTitle(tr("Social media"));
    connect(ui->actionExit, &QAction::triggered, this, &MainWindow::Exit);
    connect(ui->actionlogout, &QAction::triggered, this, &MainWindow::Logout);
    connect(ui->actionXML, &QAction::triggered, this, &MainWindow::ImportXML);
    connect(ui->actionXML2, &QAction::triggered, this, &MainWindow::ExportXML);
    this->setStyleSheet("QMessageBox {font: 12pt ""Laksaman""}");
    this->setStyleSheet("QDialogButtonBox { dialogbuttonbox-buttons-have-icons: 0; }");

    Login();
}

MainWindow::~MainWindow()
{
    delete storage_;
    delete ui;
}
void MainWindow::info(bool v)
{
    ui->lname->setVisible(v);
    ui->lid->setVisible(v);
    ui->lfounder->setVisible(v);
    ui->lyear->setVisible(v);
    ui->lactive->setVisible(v);
    ui->photos->setVisible(v);
}
void MainWindow::info2(bool v)
{
    ui->lname2->setVisible(v);
    ui->lid2->setVisible(v);
    ui->lusers2->setVisible(v);
    ui->ladmin2->setVisible(v);
}
void MainWindow::media_details(const Social & media) {

    ui->lid->setText(QString::number(media.id));
    ui->lname->setText(QString::fromStdString(media.name));
    ui->lyear->setText(QString::number(media.year));
    ui->lfounder->setText(QString::fromStdString(media.founder));
    ui->lactive->setText(QString::number(media.active));
    social_id=media.id;
    if(!media.photo.empty())
    {
        QByteArray bar = QByteArray::fromStdString(media.photo) ;
        QPixmap pixmap;
        pixmap.loadFromData(bar);
        int w = ui->photos->width();
        int h = ui->photos->height();
        ui->photos->setPixmap(pixmap.scaled(w, h, Qt::KeepAspectRatio));
    }
    else
    {
        ui->photos->setText("no photo");
    }

}
void MainWindow::ImportXML()
{
    QString filename=QFileDialog::getOpenFileName(
                this,
                "Choose xml file",
                "",
                "*.xml");
    XmlStorage xmlstorage{filename.toStdString()};
    if(!filename.isEmpty())
    {
        QFile file(filename);
        file.open(QIODevice::ReadOnly);
        QString block = file.readAll();
        ui->listWidget->clear();
        vector<Social> media=xmlstorage.loadMedia(block);
        for(Social it:media)
        {
            int id= storage_->insertMedia(it, user_id);
            if(id>0)
            {
                storage_->addUserid(user_id, id);
            }
            else if(id==-1)
            {
                storage_->updateMedia(it);
            }
        }
        vector<Social> allmedia=storage_->AllUserMedia(user_id);
        for(Social it:allmedia)
        {
            vector<Groups> group=xmlstorage.loadGroup(block, it);
            for(Groups tmp:group)
            {
                tmp.id=storage_->insertGroup(tmp);
                if(tmp.id!=0)
                {
                    storage_->insertSocialGroups(it.id, tmp.id);
                }
            }
        }
        file.close();
        ui->page->setText("1");
        ui->page1->setText("1");
        pagin(1);
        pagination1(1);
    }
    else
        QMessageBox::information(this, "Error", "File isn't opened");

}

void MainWindow::ExportXML()
{
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::Directory);
    QString current_dir = QDir::currentPath();
    QString default_name = "xmlfile.xml";
    QString filename= dialog.getSaveFileName(this, "Select New Storage Folder", current_dir + "/" + default_name, "Folders");
    if(!filename.isEmpty())
    {
        XmlStorage xmlstorage{filename.toStdString()};
        vector<Social> media=storage_->AllUserMedia(user_id);
        QFile infile(filename);
        infile.open(QIODevice::WriteOnly | QIODevice::Truncate);
        infile.close();
        for(const Social & sm : media)
        {
            vector<Groups> gr=storage_->getAllSocialGroups(sm.id);
            xmlstorage.saveMedia(sm ,gr, filename);
        }
        if(infile.open(QIODevice::ReadWrite))
        {
            QTextStream writeStream(&infile);
            QString xml = infile.readAll();
            infile.resize(0);
            writeStream<<"<Social>"+xml;
            infile.close();
        }
    }
    else
        QMessageBox::information(this, "Error", "File isn't opened");

}
void MainWindow::on_listWidget_itemClicked(QListWidgetItem *item) {

    info(true);
    ui->Edit->setEnabled(true);
    ui->remove->setEnabled(true);
    QVariant var = item->data(Qt::UserRole);
    Social tmp = var.value<Social>();
    media_details(tmp);
    ui->page22->setText("1");
    pagination2(1);
}


void MainWindow::setenabled(bool v)
{
    if(v)
    {
        ui->page->setText("1");
        ui->page1->setText("1");
        ui->page22->setText("1");
    }
    else
    {
        ui->page->setText("");
        ui->page22->setText("");
        ui->allpages->setText("");
        ui->allpages1->setText("");
        ui->allpages2->setText("");
        ui->page1->setText("");
    }
    ui->Add->setEnabled(v);
    ui->page->setEnabled(v);
    ui->page22->setEnabled(v);
    ui->find->setEnabled(v);
    ui->find2->setEnabled(v);
    ui->page1->setEnabled(v);
    ui->actionlogout->setEnabled(v);
}
void MainWindow::Login()
{
    if (storage_ != nullptr)
    {
        delete storage_;
    }
    RemoteStorage *st=new  RemoteStorage("127.0.0.1", 8080);
    storage_= st;
    login log(this, storage_);
    log.setWindowTitle("Authentication");
    int status= log.exec();
    if(status==1)
    {
        this->show();
        user_id = log.getUser().id;
        ui->listWidget->clear();
        ui->listWidget1->clear();
        ui->lfullname->setText(QString::fromStdString(log.getUser().fullname));
        setenabled(true);
        pagination2(1);
        pagination1(1);
        pagin(1);
    }
    else
    {
        this->close();
    }
}
void MainWindow::Logout()
{
    info(false);
    info2(false);
    setenabled(false);
    ui->listWidget->clear();
    ui->listWidget1->clear();
    ui->listWidget2->clear();
    user_id = 0;
    delete storage_;
    storage_=nullptr;
    this->close();
    Login();
}
void MainWindow::on_Add_clicked()
{
    if (storage_ != nullptr)
    {
        Dialog addDialog(this,storage_->allGroups(), storage_->getAllSocialGroups(0));
        addDialog.setWindowTitle("Adding");
        addDialog.listofgroups();
        int status = addDialog.exec();
        if (status == 1)
        {
            Social media = addDialog.MainSMedia();
            int id= storage_->insertMedia(media, user_id);
            if(id!=0)
            {
                storage_->addUserid(user_id, id);
                media.id = id;
                int cur_page=ui->page->text().toInt();
                pagin(cur_page);
                vector<Groups> tmp=addDialog.getinsertedgroups();
                for(Groups gr: tmp)
                {
                    if (gr.id != 0)
                    {
                        storage_->insertSocialGroups(media.id, gr.id);
                    }
                    else
                    {
                        QMessageBox::information(this, "Smth went wrong", "Can't insert this group");
                    }
                }
                vector<Groups> tmp2=addDialog.getremovedgroups();
                for(Groups gr :tmp2)
                {
                    storage_->removeSocialGroups(media.id,gr.id) ;
                }
                pagination2(1);
                QMessageBox::information(this, "Inserted", "New media id: " + QString::number(id));
            }
            else
                QMessageBox::information(this, "Not inserted", "Social media with this name alread exists");

        }
    }
}

void MainWindow::on_Edit_clicked()
{
    QList<QListWidgetItem *> items = ui->listWidget->selectedItems();
    if(items.count() !=0)
    {

        QListWidgetItem * selectedItem = items.at(0);
        QVariant var = selectedItem->data(Qt::UserRole);
        Social tmp = var.value<Social>();
        Dialog editDialog(this,storage_->allGroups(), storage_->getAllSocialGroups(tmp.id));
        editDialog.setWindowTitle("Editing");
        editDialog.EditLabels(tmp);
        int status = editDialog.exec();
        if (status == 1)
        {
            Social media = editDialog.MainSMedia();
            media.id = tmp.id;
            vector<Groups> group=editDialog.getinsertedgroups();
            for(Groups gr: group)
            {
                if (gr.id != 0)
                {
                    bool inserted = storage_->insertSocialGroups(media.id, gr.id);
                    if (!inserted)
                    {
                        QMessageBox::information(this, "Smth went wrong", "Can't insert this group");
                    }
                    else
                    {
                        int cur_page=ui->page22->text().toInt();
                        pagination2(cur_page);
                    }
                }
                else
                {
                    QMessageBox::information(this, "Not inserted", "This group is already exist");
                }
            }
            vector<Groups> group2=editDialog.getremovedgroups();
            for(Groups gr :group2)
            {
                storage_->removeSocialGroups(media.id,gr.id) ;
            }

            bool updated = storage_->updateMedia(media);
            ui->page22->setText("1");
            pagination2(1);
            if(updated)
            {
                selectedItem->setText(QString::fromStdString(media.name));
                QVariant tmpvar = QVariant::fromValue(media);
                selectedItem->setData(Qt::UserRole, tmpvar);
                media_details(media);
            }
            else {
                QMessageBox::warning(this, "Error", "Can't update this social media.");
            }
        }
    }
    else
    {
        QMessageBox::warning(
                    this,
                    "Choose an item",
                    "You didnt't choose anything to edit");
        ui->Edit->setEnabled(false);
        ui->remove->setEnabled(false);
    }
}

void MainWindow::on_remove_clicked()
{
    QList<QListWidgetItem *> items = ui->listWidget->selectedItems();
    if (items.count() != 0)
    {
        QMessageBox::StandardButton reply;
        reply = QMessageBox::question(
                    this,
                    "On remove",
                    "Are you sure?");
        if (reply == QMessageBox::StandardButton::Yes)
        {
            QListWidgetItem * selectedItem = items.at(0);
            QVariant var = selectedItem->data(Qt::UserRole);
            Social tmp = var.value<Social>();
            vector<Groups> links = storage_->getAllSocialGroups(tmp.id);
            for (Groups & group : links)
            {
                storage_->removeSocialGroups(tmp.id, group.id);
            }
            storage_->removeMedia(tmp.id);
            int page=ui->page->text().toInt();
            QString text=ui->find->text();
            int quantity=storage_->items_total(user_id, text.toStdString());
            if(quantity%3==0 && page!=1)
            {
                page--;
                ui->page->setText(QString::number(page));
            }
            pagin(page);
            pagination2(1);
            info(false);
        }
    }
    else
    {
        QMessageBox::warning(
                    this,
                    "Choose an item",
                    "You didnt't choose anything to delete");
        ui->Edit->setEnabled(false);
        ui->remove->setEnabled(false);
    }
}


void MainWindow::pagin(int const_page)
{
    ui->listWidget->clear();
    QString text= ui->find->text();
    vector<Social> media= storage_->getAllUserSocial(const_page, user_id,text.toStdString());
    for(auto it=media.begin(); it!=media.end(); it++)
    {
        QListWidget * listWidget=ui->listWidget;
        QVariant var = QVariant::fromValue((*it));
        QListWidgetItem * new_item = new QListWidgetItem();
        new_item->setText(QString::fromStdString(it->name));
        new_item->setData(Qt::UserRole, var);
        listWidget->addItem(new_item);
    }
    int quantity=storage_->items_total(user_id,text.toStdString());
    int all_pages=static_cast<int>(ceil((quantity-1)/3)+1);
    ui->allpages->setText(QString::number(all_pages));
    if(const_page==1)
    {
        ui->back->setEnabled(false);
    }
    else
    {
        ui->back->setEnabled(true);
    }
    if(const_page==all_pages)
    {
        ui->next->setEnabled(false);
    }
    else
    {
        ui->next->setEnabled(true);
    }
}
void MainWindow::on_next_clicked()
{
    int const_page=ui->page->text().toInt();
    const_page++;
    ui->page->setText(QString::number(const_page));
    pagin(const_page);
    pagination2(1);
    info(false);
}
void MainWindow::on_back_clicked()
{
    int const_page=ui->page->text().toInt();
    const_page--;
    ui->page->setText(QString::number(const_page));
    pagin(const_page);
    pagination2(1);
    info(false);
}


void MainWindow::on_find_textChanged()
{
    pagin(1);
    ui->page->setText("1");
    info(false);
}

////////////////////////////////////////


void MainWindow::pagination1(int const_page)
{
    ui->listWidget1->clear();
    QString text= ui->find2->text();
    vector<Groups> group= storage_->getAllGroups(const_page,text.toStdString());
    for(auto it=group.begin(); it!=group.end(); it++)
    {
        QListWidget * listWidget=ui->listWidget1;
        QVariant var = QVariant::fromValue((*it));
        QListWidgetItem * new_item = new QListWidgetItem();
        new_item->setText(QString::fromStdString(it->name));
        new_item->setData(Qt::UserRole, var);
        listWidget->addItem(new_item);
    }
    int quantity=storage_->groups_total(text.toStdString());
    int all_pages=ceil((quantity-1)/4)+1;
    ui->allpages1->setText(QString::number(all_pages));
    if(const_page==1)
    {
        ui->back1->setEnabled(false);
    }
    else
    {
        ui->back1->setEnabled(true);
    }
    if(const_page==all_pages)
    {
        ui->next1->setEnabled(false);
    }
    else
    {
        ui->next1->setEnabled(true);
    }
}
void MainWindow::pagination2(int const_page)
{
    ui->listWidget2->clear();
    QString  text=ui->find2->text();
    int quantity=storage_->groups_total2(social_id, text.toStdString());
    int all_pages=ceil((quantity-1)/2)+1;
    ui->allpages2->setText(QString::number(all_pages));
    vector<Groups> group=storage_->getAllSocialGroups(social_id);
    vector<Groups> gr=storage_->getSocialGroups(group,const_page, text.toStdString());
    for(auto it=gr.begin(); it!=gr.end(); it++)
    {
        QListWidget * listWidget=ui->listWidget2;
        QVariant var = QVariant::fromValue((*it));
        QListWidgetItem * new_item = new QListWidgetItem();
        new_item->setText(QString::fromStdString(it->name));
        new_item->setData(Qt::UserRole, var);
        listWidget->addItem(new_item);
    }
    if(const_page==1)
    {
        ui->back2->setEnabled(false);
    }
    else
    {
        ui->back2->setEnabled(true);
    }
    if(const_page==all_pages)
    {
        ui->next2->setEnabled(false);
    }
    else
    {
        ui->next2->setEnabled(true);
    }
}


void MainWindow::on_find2_textChanged()
{
    pagination1(1);
    pagination2(1);
    ui->page1->setText("1");
    ui->page22->setText("1");
    info2(false);
}

void MainWindow::on_next1_clicked()
{

    int const_page=ui->page1->text().toInt();
    const_page++;
    ui->page1->setText(QString::number(const_page));
    info2(false);
    pagination1(const_page);
}

void MainWindow::on_back1_clicked()
{
    int const_page=ui->page1->text().toInt();
    const_page--;
    ui->page1->setText(QString::number(const_page));
    info2(false);
    pagination1(const_page);
}

void MainWindow::on_next2_clicked()
{

    int const_page=ui->page22->text().toInt();
    const_page++;
    ui->page22->setText(QString::number(const_page));
    pagination2(const_page);
}

void MainWindow::on_back2_clicked()
{
    int const_page=ui->page22->text().toInt();
    const_page--;
    ui->page22->setText(QString::number(const_page));
    pagination2(const_page);
}

void MainWindow::on_page1_textChanged(const QString &arg1)
{
    int const_page=arg1.toInt();
    pagination1(const_page);
    info2(false);
}



void MainWindow::on_page_textChanged(const QString &arg1)
{
    int const_page=arg1.toInt();
    pagin(const_page);
    info(false);
}

void MainWindow::on_page22_textChanged(const QString &arg1)
{
    int const_page=arg1.toInt();
    pagination2(const_page);
}

void MainWindow::on_listWidget1_itemClicked(QListWidgetItem *item)
{
    ui->edit2->setEnabled(true);
    ui->remove2->setEnabled(true);
    info2(true);
    QVariant var = item->data(Qt::UserRole);
    Groups tmp = var.value<Groups>();
    ui->lid2->setText(QString::number(tmp.id));
    ui->lname2->setText(QString::fromStdString(tmp.name));
    ui->lusers2->setText(QString::number(tmp.users));
    ui->ladmin2->setText(QString::fromStdString(tmp.admin));
}


void MainWindow::on_create_clicked()
{
    if (storage_ != nullptr)
    {
        newgroup create(this);
        create.setWindowTitle("Adding");
        int status = create.exec();
        if (status == 1)
        {
            Groups group = create.MainSMedia();
            int id= storage_->insertGroup(group);
            if(id>0)
            {
                pagination1(ui->page1->text().toInt());
                QMessageBox::information(this, "Success","The new group has been created");
            }
            else QMessageBox::information(this, "Group wasn't created","There are already the same group");
        }
    }
}

void MainWindow::on_edit2_clicked()
{
    QList<QListWidgetItem *> items = ui->listWidget1->selectedItems();
    if(items.count() !=0)
    {
        QListWidgetItem * selectedItem = items.at(0);
        QVariant var = selectedItem->data(Qt::UserRole);
        Groups tmp = var.value<Groups>();
        newgroup editDialog(this);
        editDialog.setWindowTitle("Editing");
        editDialog.EditLabels(tmp);
        int status = editDialog.exec();
        if (status == 1)
        {
            Groups group = editDialog.MainSMedia();
            group.id = tmp.id;
            bool updated = storage_->updateGroup(group);
            if(updated)
            {
                selectedItem->setText(QString::fromStdString(group.name));
                QVariant tmpvar = QVariant::fromValue(group);
                selectedItem->setData(Qt::UserRole, tmpvar);
                ui->lname2->setText(QString::fromStdString(group.name));
                ui->lid2->setText(QString::number(group.id));
                ui->lusers2->setText(QString::number(group.users));
                ui->ladmin2->setText(QString::fromStdString(group.admin));
            }
            else {QMessageBox::warning(this, "Error", "Can't update this social media.");}
        }
    }
    else
    {
        QMessageBox::warning(
                    this,
                    "Choose an item",
                    "You didnt't choose anything to edit");
        ui->edit2->setEnabled(false);
        ui->remove2->setEnabled(false);
    }
}

void MainWindow::on_remove2_clicked()
{
    QList<QListWidgetItem *> items = ui->listWidget1->selectedItems();
    if (items.count() != 0)
    {
        QMessageBox::StandardButton reply;
        reply = QMessageBox::question(
                    this,
                    "Delete Group",
                    "Are you sure?",
                    QMessageBox::Yes|QMessageBox::No);
        if (reply == QMessageBox::Yes)
        {

            QListWidgetItem * selectedItem = items.at(0);
            int itemRow = ui->listWidget1->row(selectedItem);
            ui->listWidget1->takeItem(itemRow);
            QVariant var = selectedItem->data(Qt::UserRole);
            Groups tmp = var.value<Groups>();
            storage_->removeSocialGroups(social_id, tmp.id) ;
            storage_->removeGroup(tmp.id) ;
            int page=ui->page1->text().toInt();
            QString text=ui->find->text();
            int quantity=storage_->groups_total(text.toStdString());
            if(quantity%2==0 && page!=1)
            {
                page--;
                ui->page1->setText(QString::number(page));
            }
            pagination1(page);
            pagination2(1);
            ui->page22->setText("1");
            info2(false);
        }

    }
}


void MainWindow::on_actionInfo_triggered()
{
    infoWindow getinfo(this);
    getinfo.setWindowTitle("About us");
    getinfo.exec();
}
void MainWindow::Exit()
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(
                this,
                "This program will be closed",
                "Are you sure?");
    if (reply == QMessageBox::StandardButton::Yes)
        exit(1);
}


