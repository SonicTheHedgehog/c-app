#ifndef NEWGROUP_H
#define NEWGROUP_H
#include "grups.h"
#include <QDialog>

namespace Ui {
class newgroup;
}

class newgroup : public QDialog
{
    Q_OBJECT

public:
    explicit newgroup(QWidget *parent = 0);
    ~newgroup();

    Groups MainSMedia();
    void EditLabels(const Groups &group);
private:
    Ui::newgroup *ui;
};

#endif // NEWGROUP_H
