#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QMainWindow>
#include <QDebug>
#include <QAction>
#include <QFileDialog>
#include <QListWidgetItem>
#include <QMessageBox>
#include <QMainWindow>
#include "dialog.h"
#include "infowindow.h"
#include "remote_storage.h"
#include "login.h"
#include "newgroup.h"
#include "tgmath.h"
#include "xml_storage.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
private slots:
    void Logout();
    void Exit();
    void ImportXML();
    void ExportXML();
    void on_listWidget_itemClicked(QListWidgetItem *item);
    void on_Add_clicked();
    void on_Edit_clicked();
    void on_remove_clicked();
    void on_next_clicked();

    void on_back_clicked();

    void on_find_textChanged();

    void on_find2_textChanged();

    void on_next1_clicked();

    void on_back1_clicked();

    void on_next2_clicked();

    void on_back2_clicked();

    void on_page1_textChanged(const QString &arg1);

    void on_page_textChanged(const QString &arg1);

    void on_listWidget1_itemClicked(QListWidgetItem *item);

    void on_create_clicked();

    void on_edit2_clicked();

    void on_remove2_clicked();

    void on_actionInfo_triggered();

    void on_page22_textChanged(const QString &arg1);

private:
    Ui::MainWindow *ui;
    Storage *storage_;
    int user_id;
    int social_id;
    void Login();
    void info(bool v);
    void info2(bool v);
    void pagin(int page);
    void pagination1(int const_page);
    void pagination2(int const_page);
    void media_details(const Social & media);
    void setenabled(bool v);
};

#endif // MAINWINDOW_H
