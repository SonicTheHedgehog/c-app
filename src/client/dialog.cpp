#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent, vector<Groups> g, vector<Groups> tg):
    QDialog(parent),
    ui(new Ui::Dialog),
    groups(g),
    thisgroups(tg)
{
    ui->setupUi(this);
}

Dialog::~Dialog()
{
    delete ui;
}
Social Dialog::MainSMedia()
{
    Social media;
    media.name= ui->lineEdit->text().toStdString();
    media.year= ui->spinBox->value();
    media.active= ui->dSpinBox2->value();
    media.founder= ui->lineEdit2->text().toStdString();
    if(image.size()!=0)
    {
        media.photo = image.toStdString() ;
    }
    return media;
}
void Dialog::listofgroups()
{
    for(Groups & g: groups)
    {
        QVariant var = QVariant::fromValue(g);
        QListWidgetItem * new_item = new QListWidgetItem();
        new_item->setText(QString::fromStdString(g.name));
        new_item->setData(Qt::UserRole, var);
        new_item->setFlags(new_item->flags() | Qt::ItemIsUserCheckable);
        new_item->setCheckState(Qt::Unchecked);
        ui->listWidget2->addItem(new_item);
    }
}
void Dialog::EditLabels(const Social &media)
{
    ui->lineEdit->setText(QString::fromStdString(media.name));
    ui->spinBox->setValue(media.year);
    ui->dSpinBox2->setValue(media.active);
    ui->lineEdit2->setText(QString::fromStdString(media.founder));
    for(Groups & g: groups)
    {
        QVariant var = QVariant::fromValue(g);
        QListWidgetItem * new_item = new QListWidgetItem();
        new_item->setText(QString::fromStdString(g.name));
        new_item->setData(Qt::UserRole, var);
        new_item->setFlags(new_item->flags() | Qt::ItemIsUserCheckable);
        new_item->setCheckState(Qt::Unchecked);
        for(Groups & g2: thisgroups)
        {
            if(g2.id==g.id)
            {
                new_item->setCheckState(Qt::Checked);
                new_item->setBackgroundColor(QColor("#73C6B6"));
            }
        }
        ui->listWidget2->addItem(new_item);
    }
    image=QByteArray::fromStdString(media.photo);
    social_id=media.id;
    if(!media.photo.empty())
    {
        QByteArray bar = QByteArray::fromStdString(media.photo) ;
        QPixmap pixmap;
        pixmap.loadFromData(bar);
        int w = ui->photos->width();
        int h = ui->photos->height();
        ui->photos->setPixmap(pixmap.scaled(w, h, Qt::KeepAspectRatio));
    }
    else
    {
        ui->photos->setText("no photo");
        ui->delete_image->setEnabled(false);
    }
}

void Dialog::on_add_image_clicked()
{
    QString filter = "Image files (*.png *.xpm *.jpg *.jpeg *.gif)";
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::ExistingFile);
    QString path = dialog.getOpenFileName(
                this,
                "Select File", "", filter);
    if (path != "")
    {
        QFile file(path);
        if (file.open(QIODevice::ReadOnly))
        {
            image= file.readAll();
        }
    }
    if(!image.toStdString().empty())
    {
        QPixmap pixmap;
        pixmap.loadFromData(image);
        int w = ui->photos->width();
        int h = ui->photos->height();
        ui->photos->setPixmap(pixmap.scaled(w, h, Qt::KeepAspectRatio));
    }
}

vector<Groups> Dialog::getinsertedgroups()
{
    return insertedgr;
}
vector<Groups> Dialog::getremovedgroups()
{
    return removedgr;
}

void Dialog::on_delete_image_clicked()
{
    image.clear();
    ui->photos->setText("no photo");
}

void Dialog::on_listWidget2_itemClicked(QListWidgetItem *item)
{
    if(item->checkState() == Qt::Checked)
    {
        item->setBackgroundColor(QColor("#73C6B6"));
        QVariant var = item->data(Qt::UserRole);
        Groups tmp = var.value<Groups>();
        insertedgr.push_back(tmp);
    }

    else
    {
        QVariant var = item->data(Qt::UserRole);
        Groups tmp = var.value<Groups>();
        removedgr.push_back(tmp);
    }
}
