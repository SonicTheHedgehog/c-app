#ifndef DIALOG_H
#define DIALOG_H
#include "social.h"
#include "remote_storage.h"
#include <QDialog>
#include <QFile>
#include <QFileDialog>
#include <QListWidget>
namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT


public:
     Dialog(QWidget *parent, vector<Groups> g, vector<Groups> tg);
    ~Dialog();

    Social MainSMedia();
    void listofgroups();
    void EditLabels(const Social &media);
    vector<Groups> getinsertedgroups();
    vector<Groups> getremovedgroups();
private slots:
    void on_add_image_clicked();

    void on_delete_image_clicked();

    void on_listWidget2_itemClicked(QListWidgetItem *item);

private:
    Ui::Dialog *ui;
    Storage *storage_;
    QByteArray image;
    int social_id;
   vector<Groups> groups;
   vector<Groups> thisgroups;
    vector<Groups> removedgr;
    vector<Groups> insertedgr;
};

#endif // DIALOG_H
