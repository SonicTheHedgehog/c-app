#include "login.h"
#include "ui_login.h"

login::login(QWidget *parent, Storage *storage) :
    QDialog(parent),
    ui(new Ui::login),
    storage_(storage)
{
    ui->setupUi(this);
    ui->label_3->setVisible(false);
}

login::~login()
{
    delete ui;

}
Users login::getUser()
{
    return cur_user;
}
QString login::hashPassword(QString const & password)
{

    QByteArray pass_ba = password.toUtf8();
    QByteArray hash_ba = QCryptographicHash::hash(pass_ba, QCryptographicHash::Md5);
    QString pass_hash = QString(hash_ba.toHex());
    return pass_hash;

}
void login::on_oklog_clicked()
{
    try
    {
        string username = ui->login2->text().toStdString();
        string password = ui->password->text().toStdString();
        if(username==""|| password=="")
        {
            QMessageBox::information(this, "Fail log in", "You need to fill in all fields");
        }
        else
        {
            string hashed_pass = hashPassword(QString::fromStdString(password)).toStdString();
            Users tmp = storage_->getUserAuth(username, hashed_pass);
            if (tmp.id!=-1)
            {
                cur_user=tmp;
                this->done(1);
                this->close();
            }
            else
            {
                ui->label_3->setVisible(true);
                QMessageBox::information(this, "Error", "No such users found, please, try again.");
            }
        }

    }
    catch(...)
    {
        QMessageBox::warning(this, " Error","Server hasn't been opened.");
        exit(1);
    }
}

void login::on_nolog_clicked()
{

    try
    {
        this->done(0);
        this->close();
    }
    catch(...)
    {
        QMessageBox::warning(this, " Error","Server hasn't been opened.");
        exit(1);
    }
}

void login::on_reg_clicked()
{   registration newuser(this, storage_);
    newuser.setWindowTitle("Registration");
    int status =newuser.exec();
    if(status==1)
    {
        this->close();
        cur_user=storage_->getUserbyid(newuser.newid);
        if(cur_user.id!=-1)
        {
            Users tmp = storage_->getUserAuth(cur_user.username, cur_user.password_hash);
            if (tmp.id!=-1)
            {
                this->done(1);
            }
            else
            {
                QMessageBox::information(this, "Error", "Someting went wrong");
                this->done(0);
            }
        }
    }

}
