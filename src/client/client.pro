#-------------------------------------------------
#
# Project created by QtCreator 2020-05-20T10:27:23
#
#-------------------------------------------------

QT       += core gui
CONFIG += c++1z
QT +=  sql
QT+=xml
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = client
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp\
      dialog.cpp\
      login.cpp\
      newgroup.cpp\
      registration.cpp\
    remote_storage.cpp\
        xml_storage.cpp \
    infowindow.cpp


HEADERS += \
        mainwindow.h\
      dialog.h\
      login.h\
      newgroup.h\
      registration.h\
    remote_storage.h\
        xml_storage.h \
    infowindow.h

FORMS += \
        mainwindow.ui\
        dialog.ui\
        login.ui\
        newgroup.ui\
        registration.ui\
    infowindow.ui




win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../storage/release/ -lstorage
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../storage/debug/ -lstorage
else:unix: LIBS += -L$$OUT_PWD/../storage/ -lstorage

INCLUDEPATH += $$PWD/../storage
DEPENDPATH += $$PWD/../storage

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../storage/release/libstorage.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../storage/debug/libstorage.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../storage/release/storage.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../storage/debug/storage.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../storage/libstorage.a

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../rpclib-master/release/ -lrpc
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../rpclib-master/debug/ -lrpc
else:unix: LIBS += -L$$PWD/../../../rpclib-master/ -lrpc

INCLUDEPATH += $$PWD/../../../rpclib-master
DEPENDPATH += $$PWD/../../../rpclib-master

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../rpclib-master/release/librpc.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../rpclib-master/debug/librpc.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../rpclib-master/release/rpc.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../rpclib-master/debug/rpc.lib
else:unix: PRE_TARGETDEPS += $$PWD/../../../rpclib-master/librpc.a
