#ifndef REMOTE_STORAGE_H
#define REMOTE_STORAGE_H
#include "storage.h"
#include <rpc/client.h>
#include <QDebug>
using namespace std;
class RemoteStorage:public Storage
{
    rpc::client * cl;
public:
    RemoteStorage(const string & address, int port_id);
    ~RemoteStorage();
    void isConnect();
    bool isOpen(){return true;}
    bool open(){return true;}
    void close(){}

    vector<Social> getAllMedia()  ;
     vector<Social> AllUserMedia(int user_id)  ;
    bool updateMedia(const Social &media) ;
    bool removeMedia(int id)  ;
    int insertMedia(const Social &media, int user_id)  ;
     Groups getGroupById(int id)  ;
    bool updateGroup(const Groups &group)  ;
    bool removeGroup(int id)  ;
    int insertGroup(const Groups &group)  ;
    vector<Groups> allGroups()  ;
    // users
    Users getUserAuth( const string & username, const string & password)  ;
    vector<Social> getAllUserSocial(const int const_page, int user_id, string text)  ;
    int items_total(int user_id, string text)  ;
    bool addUserid(int user_id, int gid)  ;
    int insertUser(const Users &user)  ;
    Users getUserbyid(const int user_id)  ;
    // links
    vector<Groups> getAllSocialGroups(int social_id)  ;
    bool insertSocialGroups(int social_id, int group_id)  ;
    bool removeSocialGroups(int social_id, int group_id)  ;
    vector<Groups> getAllGroups(const int const_page,string text);
    int groups_total(string text)  ;
    vector<Groups> getSocialGroups(vector<Groups> gr, int const_page, string text)  ;
    int groups_total2(int social_id, string text)  ;
   bool registr(Users u);
};

#endif // REMOTE_STORAGE_H
