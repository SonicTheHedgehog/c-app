#include "remote_storage.h"
#include <rpc/this_session.h>
RemoteStorage::RemoteStorage( const string &address, int port_id)
{
    cl=new rpc::client(address, port_id);
}
RemoteStorage::~RemoteStorage()
{
    rpc::this_session().post_exit();
    delete cl;
}


void RemoteStorage::isConnect()
{
    rpc::client::connection_state connection_state = cl->get_connection_state();
    if(connection_state != rpc::client::connection_state::connected)
    {
        throw 1;
    }
}


vector<Social> RemoteStorage::getAllMedia()
{
    isConnect();
    auto result = cl->call("getAllMedia").as<vector<Social>>();
    return result;
}
vector<Social> RemoteStorage::AllUserMedia(int user_id)
{
    isConnect();
    auto result = cl->call("AllUserMedia",user_id).as<vector<Social>>();
    return result;
}
int RemoteStorage::insertMedia(const Social &media, int user_id)
{
    isConnect();
       auto result = cl->call("insertMedia",media, user_id).as<int>();
    return result;

}

bool RemoteStorage::removeMedia(int id)
{

    isConnect();
      auto result = cl->call("removeMedia",id).as<bool>();
    return result;

}
bool RemoteStorage::updateMedia(const Social &media)
{
    isConnect();
          auto result = cl->call("updateMedia",media).as<bool>();
    return result;
}
vector<Groups> RemoteStorage::allGroups()
{
    isConnect();
    auto result = cl->call("allGroups").as<vector<Groups>>();
    return result;
}
Groups RemoteStorage::getGroupById(int id)
{
    isConnect();
    auto result = cl->call("getGroupById",id).as<Groups>();
    return result;
}

bool RemoteStorage::updateGroup(const Groups &group)
{
    isConnect();
     auto result = cl->call("updateGroup",group).as<bool>();
    return result;
}

bool RemoteStorage::removeGroup(int id)
{
    isConnect();
     auto result = cl->call("removeGroup",id).as<bool>();
    return result;
}

int RemoteStorage::insertGroup(const Groups &group)
{
    isConnect();
    auto result = cl->call("insertGroup",group).as<int>();
    return result;
}

Users RemoteStorage::getUserAuth( const string & username, const string & password)
{
    isConnect();
  auto result = cl->call("getUserAuth",username,  password).as<Users>();
    return result;
}
vector<Social> RemoteStorage::getAllUserSocial(const int const_page, int user_id, string text)
{

    isConnect();
    auto result = cl->call("getAllUserSocial",const_page, user_id, text).as<vector<Social>>();
    return result;
}
int RemoteStorage::items_total(int user_id, string text)
{
    isConnect();
    auto result = cl->call("items_total",user_id, text).as<int>();
    return result;
}
bool RemoteStorage::addUserid(int user_id, int gid)
{
    isConnect();
    auto result = cl->call("addUserid", user_id, gid).as<bool>();
    return result;
}
int RemoteStorage::insertUser(const Users &user)
{
    isConnect();
    auto result = cl->call("insertUser", user).as<int>();
    return result;
}
Users RemoteStorage::getUserbyid(const int user_id)
{
    isConnect();
    auto result = cl->call("getUserbyid",user_id).as<Users>();
    return result;
}

vector<Groups> RemoteStorage::getAllSocialGroups( int social_id)
{
    isConnect();
    auto result = cl->call("getAllSocialGroups",social_id).as<vector<Groups>>();
    return result;
}

bool RemoteStorage::insertSocialGroups(int social_id, int group_id)
{
    isConnect();
    auto result = cl->call("insertSocialGroups",social_id, group_id).as<bool>();
    return result;
}

bool RemoteStorage::removeSocialGroups(int social_id, int group_id)
{
    isConnect();
    auto result = cl->call("removeSocialGroups",social_id,  group_id).as<bool>();
    return result;
}



vector<Groups> RemoteStorage::getAllGroups(const int const_page, string text)
{
    isConnect();
    auto result = cl->call("getAllGroups",const_page, text).as<vector<Groups>>();
    return result;
}

int RemoteStorage::groups_total(string text)
{
    isConnect();
    auto result = cl->call("groups_total",text).as<int>();
    return result;
}

vector<Groups> RemoteStorage::getSocialGroups(vector<Groups> gr, int const_page, string text)
{
    isConnect();
    auto result = cl->call("getSocialGroups",gr,const_page, text).as<vector<Groups>>();
    return result;

}
int RemoteStorage::groups_total2(int social_id, string text)
{
    isConnect();
    auto result = cl->call("groups_total2",social_id, text).as<int>();
    return result;
}
bool  RemoteStorage::registr(Users u)
{
    isConnect();
    auto result = cl->call("registr",u).as<bool>();
    return result;
}
