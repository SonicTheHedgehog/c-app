#ifndef XML_STORAGE_H
#define XML_STORAGE_H
#include "social.h"
#include "grups.h"
#include <QDebug>
#include <QtXml>
#include <QString>
#include <iostream>
#include <QFile>
#include  <QTextStream>
using namespace std;

class XmlStorage
{
    string dir_name_;

public:
    explicit XmlStorage(const string & dir_name) {dir_name_=dir_name;}
    vector<Social> loadMedia(QString xmlText);
    void saveMedia(const Social med, const vector<Groups> &group, QString file);
    vector<Groups> loadGroup(QString xmlText, Social media);
};


#endif // XML_STORAGE_H
