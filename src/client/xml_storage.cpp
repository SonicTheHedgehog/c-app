#include "xml_storage.h"

Social xmlElementToSocial(QDomElement & media);
QDomElement socialToXmlElement(QDomDocument & doc, const Social &media);
Groups xmlElementToGroups(QDomElement & group);
QDomElement groupToXmlElement(QDomDocument &doc, const Groups &sg);
vector<Social> XmlStorage::loadMedia(QString xmlText)
{
    std::vector<Social> media;
    std::vector<Groups> group;
    QDomDocument doc;
    QString errorMessage;
    if (!doc.setContent(xmlText, &errorMessage)) {
        qDebug() << "error parsing xml: " << errorMessage;
        exit(1);
    }
    QDomElement rootEl = doc.documentElement();
    QDomNodeList rootElChildren = rootEl.childNodes();
    for (int i = 0; i < rootElChildren.length(); i++) {
        QDomNode socialNode = rootElChildren.at(i);
        QDomElement socialEl = socialNode.toElement();
        Social sm = xmlElementToSocial(socialEl);
        media.push_back(sm);
    }
    return media;

}
vector<Groups> XmlStorage::loadGroup(QString xmlText, Social media)
{
    std::vector<Groups> group;
    QDomDocument doc;
    QString errorMessage;
    if (!doc.setContent(xmlText, &errorMessage)) {
        qDebug() << "error parsing xml: " << errorMessage;
        exit(1);
    }
    QDomElement rootEl = doc.documentElement();
    QDomNodeList rootElChildren = rootEl.childNodes();
    for (int i = 0; i < rootElChildren.length(); i++)
    {
        QDomNode socialNode = rootElChildren.at(i);
        QDomElement socialEl = socialNode.toElement();
        Social s=xmlElementToSocial(socialEl);
        if(s.name==media.name)
        {
            QDomNodeList secondchild=socialEl.childNodes();
            for (int i = 0; i < secondchild.length(); i++) {
                QDomNode groupNode2 = secondchild.at(i);
                QDomElement groupEl2 = groupNode2.toElement();
                Groups sm = xmlElementToGroups(groupEl2);
                group.push_back(sm);
            }
        }
    }

    return group;
}
void XmlStorage::saveMedia( const Social med, const vector<Groups> &group, QString file)
{
    QDomDocument doc;
    QDomElement groupEl;
    QDomElement rootEl = doc.createElement("Social");
    QDomElement socialEl= socialToXmlElement(doc, med);
    rootEl.appendChild(socialEl);
    for(Groups gr: group)
    {
        QDomElement groupEl = groupToXmlElement(doc, gr);
        socialEl.appendChild(groupEl);
    }
    doc.appendChild(rootEl);
    QString line=doc.toString(4);
    QFile filein(file);
    filein.open(QIODevice::ReadOnly );
    QString block = filein.readAll();
    block.chop(10);
    line=line.mid(8);
    block+=line;
    QFile fileOut(file);
    if(fileOut.open(QIODevice::WriteOnly | QIODevice::Text))
    {

        QTextStream writeStream(&fileOut);
        writeStream <<block;
        fileOut.close();
    }
}
QDomElement socialToXmlElement(QDomDocument & doc, const Social &media)
{
    QDomElement socialEl = doc.createElement("media");
    socialEl.setAttribute("name", QString::fromStdString(media.name));
    socialEl.setAttribute("year", QString::number(media.year));
    socialEl.setAttribute("founder", QString::fromStdString(media.founder));
    socialEl.setAttribute("active", QString::number(media.active));
    socialEl.setAttribute("id", media.id);
    return socialEl ;
}
Social xmlElementToSocial(QDomElement & media)
{
    Social social;
    social.name=media.attribute("name").toStdString();
    social.year=media.attribute("year").toInt();
    social.founder=media.attribute("founder").toStdString();
    social.active=media.attribute("active").toInt();
    social.id=media.attribute("id").toInt();
    social.photo=media.attribute("photo").toStdString();
    return social;
}
Groups xmlElementToGroups(QDomElement & group)
{
    Groups gr;
    gr.name=group.attribute("name").toStdString();
    gr.users=group.attribute("users").toInt();
    gr.admin=group.attribute("admin").toStdString();
    gr.id=group.attribute("id").toInt();
    return gr;
}
QDomElement groupToXmlElement(QDomDocument & doc, const Groups &sg)
{
    QDomElement groupEl= doc.createElement("group");
    groupEl.setAttribute("name", QString::fromStdString(sg.name));
    groupEl.setAttribute("users", QString::number(sg.users));
    groupEl.setAttribute("admin", QString::fromStdString(sg.admin));
    groupEl.setAttribute("id", sg.id);

    return groupEl ;
}
