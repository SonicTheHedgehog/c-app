#ifndef USER_H
#define USER_H
#include <rpc/msgpack.hpp>
#include <QVariant>
#include <string>
using namespace std;
struct Users
{
    string username;
    string password_hash;
    string fullname;
    int id;
    MSGPACK_DEFINE(id, username, password_hash,  fullname)
};
Q_DECLARE_METATYPE(Users)
#endif // USER_H
