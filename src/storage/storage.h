#ifndef STORAGE_H
#define STORAGE_H

#include <string>
#include <vector>

#include "social.h"
#include "grups.h"
#include "user.h"

using namespace std;
class Storage
{
private:
    string dir_name_;

public:
    explicit Storage(const string & dir_name="");
    virtual ~Storage(){}

    void setName(const string & dir_name);
    string name() const;

    virtual bool isOpen()= 0;
    virtual bool open() = 0;
    virtual void close() = 0;

    virtual vector<Social> getAllMedia(void) = 0;
     virtual vector<Social> AllUserMedia(int user_id) = 0;
    virtual bool updateMedia(const Social &media) = 0;
    virtual bool removeMedia(int id) = 0;
    virtual int insertMedia(const Social &media, int user_id) = 0;
    virtual  Groups getGroupById(int id)= 0;
    virtual bool updateGroup(const Groups &group)= 0;
    virtual bool removeGroup(int id)= 0;
    virtual int insertGroup(const Groups &group)= 0;
    virtual vector<Groups> allGroups()=0;
    // users
    virtual Users getUserAuth( const string & username, const string & password) = 0;
    virtual vector<Social> getAllUserSocial(const int const_page, int user_id, string text) = 0;
    virtual int items_total(int user_id, string text)=0;
    virtual bool addUserid(int user_id, int gid)=0;
    virtual int insertUser(const Users &user)=0;
    virtual Users getUserbyid(const int user_id)=0;
    // links
    virtual vector<Groups> getAllSocialGroups(int social_id) = 0;
    virtual bool insertSocialGroups(int social_id, int group_id) = 0;
    virtual bool removeSocialGroups(int social_id, int group_id) = 0;
    virtual vector<Groups> getAllGroups(const int const_page,string text)= 0;
    virtual int groups_total(string text)=0;
    virtual vector<Groups> getSocialGroups(vector<Groups> gr, int const_page, string text)=0;
    virtual int groups_total2(int social_id, string text)=0;
   virtual bool registr(Users u)=0;
};

#endif // STORAGE_H
