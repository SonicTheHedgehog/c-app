#ifndef GRUPS_H
#define GRUPS_H
#include <rpc/msgpack.hpp>
#include <QVariant>
#include <string>
using namespace std;
struct Groups
{
    int id;
    string name;
    int users;
    string admin;
    MSGPACK_DEFINE(id, name, users,  admin)
};
Q_DECLARE_METATYPE(Groups)
#endif // GRUPS_H
