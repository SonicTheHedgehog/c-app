#ifndef SOCIAL_H
#define SOCIAL_H
#include <rpc/msgpack.hpp>
#include <QVariant>
#include <string>
using namespace std;
struct Social
{
    string name;
    int year;
    string founder;
    int active;
    int id;
    string photo;
    MSGPACK_DEFINE(id, name, year, founder, active, photo)
};
Q_DECLARE_METATYPE(Social)
#endif // SOCIAL_H

