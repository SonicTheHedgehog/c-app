#ifndef REMOTEUI_H
#define REMOTEUI_H
#include "storage.h"
#include "rpc/server.h"
#include <iostream>
class RemoteUi
{
     Storage *storage_;
public:
    RemoteUi(Storage * storage);
    void run();

};

#endif // REMOTEUI_H
