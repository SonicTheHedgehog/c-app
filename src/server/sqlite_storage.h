#ifndef SQLITE_STORAGE_H
#define SQLITE_STORAGE_H
#include <QtSql>
#include <QSqlDatabase>
#include "storage.h"
using namespace std;
class SqliteStorage: public Storage
{
    const string dir_name_;
    QSqlDatabase db_;
public:
    SqliteStorage(const string & dir_name);//: Storage(dir_name) {db_ = QSqlDatabase::addDatabase("QSQLITE");}

    bool isOpen() ;
     bool open()  ;
    void close();
    virtual ~SqliteStorage() override{}

    vector<Social> getAllMedia(void)  ;
     vector<Social> AllUserMedia(int user_id)  ;
    bool updateMedia(const Social &media)  ;
    bool removeMedia(int id)  ;
    int insertMedia(const Social &media, int user_id)  ;
     Groups getGroupById(int id) ;
    bool updateGroup(const Groups &group) ;
    bool removeGroup(int id) ;
    int insertGroup(const Groups &group) ;
    vector<Groups> allGroups() ;
    // users
    Users getUserAuth( const string & username, const string & password)  ;
    vector<Social> getAllUserSocial(const int const_page, int user_id, string text)  ;
    int items_total(int user_id, string text) ;
    bool addUserid(int user_id, int gid) ;
    int insertUser(const Users &user) ;
    Users getUserbyid(const int user_id) ;
    // links
    vector<Groups> getAllSocialGroups(int social_id)  ;
    bool insertSocialGroups(int social_id, int group_id)  ;
    bool removeSocialGroups(int social_id, int group_id)  ;
    vector<Groups> getAllGroups(const int const_page,string text) ;
    int groups_total(string text) ;
    vector<Groups> getSocialGroups(vector<Groups> gr, int const_page, string text) ;
    int groups_total2(int social_id, string text) ;
   bool registr(Users u);
};

#endif // SQLITE_STORAGE_H
