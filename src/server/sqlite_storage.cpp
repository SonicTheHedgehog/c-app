#include "sqlite_storage.h"

SqliteStorage::SqliteStorage(const string & dir_name): Storage(dir_name)
{
    db_ = QSqlDatabase::addDatabase("QSQLITE");
}
bool SqliteStorage::isOpen() {

    return db_.isOpen();
}

bool SqliteStorage::open(){
    QString path = QString::fromStdString(this->name()) + "/data.sqlite";
    db_.setDatabaseName(path);
    bool connected = db_.open();
    if (!connected)
    {
        return false;
    }
    return true;
}
void SqliteStorage::close()
{
    db_.close();

}
Social getMediafromQuery(const QSqlQuery &query)
{
    Social media;
    int idPhoto = query.record().indexOf("photo") ;
    QByteArray photo = query.value(idPhoto).toByteArray() ;
    media.id=query.value("id").toInt();
    media.name=query.value("name").toString().toStdString();
    media.year=query.value("year").toInt();
    media.founder=query.value("founder").toString().toStdString();
    media.active=query.value("active").toInt();
    media.photo=photo.toStdString() ;
    return media;
}

Groups getGroupfromQuery(const QSqlQuery &query)
{
    Groups gr;
    gr.id=query.value("id").toInt();
    gr.name=query.value("name").toString().toStdString();
    gr.users=query.value("users").toInt();
    gr.admin= query.value("admin").toString().toStdString();
    return gr;
}

vector<Social> SqliteStorage::getAllMedia(void)
{
    QSqlQuery query("SELECT * FROM social_media");
    vector<Social> socials;
    while (query.next())
    {
        Social s=getMediafromQuery(query);
        socials.push_back(s);
    }
    return socials;
}
vector<Social> SqliteStorage::AllUserMedia(int user_id)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM social_media WHERE user_id=:user_id");
    query.bindValue(":user_id", user_id);
    vector<Social> socials;
    if(!query.exec())
    {
        qDebug()<<"Get all user media error"<<query.lastError();

    }
    while (query.next())
    {
        Social s=getMediafromQuery(query);
        socials.push_back(s);
    }
    return socials;
}
int SqliteStorage::insertMedia(const Social &media, int user_id)
{
    QSqlQuery query;
    int count2;
    query.prepare("SELECT COUNT(*) FROM social_media WHERE name=:name AND user_id=:user_id");
    query.bindValue(":name", QString::fromStdString(media.name));
    query.bindValue(":user_id", user_id);
    if(!query.exec())
    {
        qDebug()<<"Get all user media error"<<query.lastError();
        return 0;
    }
    if (query.next())
    {
        count2=query.value(0).toInt();
    }
    if(count2==0)
    {
        query.clear();
        query.prepare("INSERT INTO social_media (name, year, founder, active, photo) VALUES (:name, :year, :founder,:active, :photo)");
        query.bindValue(":name", QString::fromStdString(media.name));
        query.bindValue(":year", media.year);
        query.bindValue(":founder",  QString::fromStdString(media.founder));
        query.bindValue(":active", media.active);
        query.bindValue(":photo", QString::fromStdString(media.photo));
        if (!query.exec())
        {
            qDebug() << "Insert social media query exec error:" << query.lastError();
            return 0;
        }
        QVariant var=query.lastInsertId();
        return var.toInt();
    }
    else
        return -1;

}

bool SqliteStorage::removeMedia(int id)
{
    QSqlQuery query;
    query.prepare("DELETE FROM social_media WHERE id = :id");
    query.bindValue(":id", id);
    if (!query.exec())
    {
        return false;
    }
    if(query.numRowsAffected()==0)
    {
        return false;
    }
    return true;

}

bool SqliteStorage::updateMedia(const Social &media)
{
    QSqlQuery query;
    QByteArray photo = photo.fromStdString(media.photo) ;
    query.prepare("UPDATE social_media SET name = :name, year=:year, founder=:founder, active=:active, photo=:photo WHERE id = :id");
    query.bindValue(":name", QString::fromStdString(media.name));
    query.bindValue(":year", media.year);
    query.bindValue(":founder", QString::fromStdString(media.founder));
    query.bindValue(":active", media.active);
    query.bindValue(":photo", photo);
    query.bindValue(":id", media.id);
    if (!query.exec())
    {
        qDebug() << "Update social media query error:" << query.lastError();
        return false;
    }
    else if(query.numRowsAffected()==0)
    {
        return false;
    }
    return true;
}
vector<Groups> SqliteStorage::allGroups()
{
    QSqlQuery query("SELECT * FROM groups");
    vector<Groups> gr;
    while (query.next())
    {
        Groups s=getGroupfromQuery(query);
        gr.push_back(s);
    }
    return gr;
}
Groups SqliteStorage::getGroupById(int gid)
{
    QSqlQuery query;
    Groups gr;
    query.prepare("SELECT * FROM groups WHERE id = :id");
    query.bindValue(":id", gid);
    if(!query.exec())
    {
        qDebug()<<"Get group error"<<query.lastError();
        gr.id=-1;
        return gr;
    }
    if(query.next())
    {
        gr=getGroupfromQuery(query);
        return gr;
    }
    else
    {
        gr.id=-1;
        return gr;
    }
}

bool SqliteStorage::updateGroup(const Groups &group)
{
    QSqlQuery query;
    query.prepare("UPDATE groups SET name = :name, users=:users, admin=:admin WHERE id = :id");
    query.bindValue(":id", group.id);
    query.bindValue(":name", QString::fromStdString(group.name));
    query.bindValue(":users", group.users);
    query.bindValue(":admin", QString::fromStdString(group.admin));
    if (!query.exec())
    {
        qDebug() << "Update group query error:" << query.lastError();
        return false;
    }
    else if(query.numRowsAffected()==0)
    {
        return false;
    }
    return true;
}

bool SqliteStorage::removeGroup(int id)
{
    QSqlQuery query;
    query.prepare("DELETE FROM groups WHERE id = :id");
    query.bindValue(":id", id);
    if (!query.exec())
    {
        return false;
    }
    if(query.numRowsAffected()==0)
    {
        return false;
    }
    return true;
}
int SqliteStorage::insertGroup(const Groups &group)
{
    QSqlQuery query;
    int count;
    query.prepare("SELECT COUNT(*) FROM groups WHERE name=:name AND users=:users AND admin=:admin");
    query.bindValue(":name", QString::fromStdString(group.name));
    query.bindValue(":users", group.users);
    query.bindValue(":admin", QString::fromStdString(group.admin));
    if(!query.exec())
    {
        qDebug()<<"Get all groups error"<<query.lastError();
        return 0;

    }
    if (query.next())
    {
        count=query.value(0).toInt();

    }
    if(count==0)
    {
        query.clear();
        query.prepare("INSERT INTO groups (name,users, admin) VALUES ( :name, :users, :admin)");
        query.bindValue(":name", QString::fromStdString(group.name));
        query.bindValue(":users", group.users);
        query.bindValue(":admin", QString::fromStdString(group.admin));
        if (!query.exec())
        {
            qDebug() << "Insert group query error:" << query.lastError();
            return 0;
        }
        QVariant var=query.lastInsertId();
        return var.toInt();
    }
    else
    {
        query.clear();
        query.prepare("SELECT * FROM groups WHERE name=:name AND users=:users AND admin=:admin");
        query.bindValue(":name", QString::fromStdString(group.name));
        query.bindValue(":users", group.users);
        query.bindValue(":admin", QString::fromStdString(group.admin));
        if(!query.exec())
        {
            qDebug()<<"Get all groups error"<<query.lastError();
            return 0;
        }
        if (query.next())
        {
            return query.value("id").toInt();
        }
        else
            return 0;
    }
}

Users SqliteStorage::getUserAuth( const string & username, const string & password)
{
    QSqlQuery query;
    Users u;
    query.prepare("SELECT * FROM users WHERE username = :username AND password_hash = :password_hash");
    query.bindValue(":username", QString::fromStdString(username));
    query.bindValue(":password_hash", QString::fromStdString(password));
    if(!query.exec())
    {
        qDebug()<<"Get user error"<<query.lastError();
        u.id=-1;
        return u;
    }
    if(query.next())
    {

        u.id=query.value("id").toInt();
        u.username=query.value("username").toString().toStdString();
        u.password_hash= query.value("password_hash").toString().toStdString();
        u.fullname = query.value("fullname").toString().toStdString();
        return u;
    }
    else
    {
        u.id=-1;
        return u;
    }
}

bool SqliteStorage::addUserid(int user_id, int idd)
{
    QSqlQuery query;
    query.prepare("UPDATE social_media SET user_id=:user_id WHERE id = :id");
    query.bindValue(":user_id", user_id);
    query.bindValue(":id", idd);
    if (!query.exec())
    {
        qDebug() << "Update users in main table error:" << query.lastError();
        return false;
    }
    else if(query.numRowsAffected()==0)
    {
        return false;
    }
    return true;
}

vector<Groups> SqliteStorage::getAllSocialGroups( int social_id)
{
    vector<Groups> groups2;
    QSqlQuery query;
    query.prepare("SELECT * FROM links WHERE social_id = :social_id");
    query.bindValue(":social_id", social_id);
    if(!query.exec())
    {
        qDebug()<<"Get link error"<<query.lastError();
    }
    while (query.next())
    {
        int group_id = query.value("group_id").toInt();
        Groups tmp = getGroupById(group_id);
        if (tmp.id!=-1)
        {
            groups2.push_back(tmp);
        }
    }
    return groups2;
}

bool SqliteStorage::insertSocialGroups(int social_id, int group_id)
{
    QSqlQuery query;
    int count;
    query.prepare("SELECT COUNT(*) FROM links WHERE social_id=:social_id AND group_id=:group_id");
    query.bindValue(":social_id", social_id);
    query.bindValue(":group_id", group_id);
    if(!query.exec())
    {
        qDebug()<<"Get all user media error"<<query.lastError();
    }
    if (query.next())
    {
        count=query.value(0).toInt();
    }
    if(count==0)
    {
        query.clear();
        query.prepare("INSERT INTO links (social_id, group_id) VALUES ( :social_id, :group_id)");
        query.bindValue(":social_id", social_id);
        query.bindValue(":group_id", group_id);
        if (!query.exec())
        {
            qDebug() << "insert links query error:" << query.lastError();
            return false;
        }
        return true;
    }
    else
        return false;
}

bool SqliteStorage::removeSocialGroups(int social_id, int group_id)
{
    QSqlQuery query;
    query.prepare("DELETE FROM links WHERE social_id=:social_id AND group_id=:group_id");
    query.bindValue(":social_id", social_id);
    query.bindValue(":group_id", group_id);
    if (!query.exec())
    {
        return false;
    }
    if(query.numRowsAffected()==0)
    {
        return false;
    }
    return true;
}
int SqliteStorage::insertUser(const Users &user)
{
    QSqlQuery query;
    if(!query.prepare("INSERT INTO users (username, password_hash, fullname) VALUES (:username, :password_hash, :fullname)"))
    {
        QSqlError error = query.lastError();
        throw error;
    }
    query.bindValue(":username", QString::fromStdString(user.username));
    query.bindValue(":password_hash", QString::fromStdString(user.password_hash));
    query.bindValue(":fullname", QString::fromStdString(user.fullname));
    if (!query.exec())
    {
        qDebug() << "Insert user query error:" << query.lastError();
        return 0;
    }
    QVariant var=query.lastInsertId();
    return var.toInt();
}
Users SqliteStorage::getUserbyid(const int user_id)
{
    Users u;
    QSqlQuery query;
    query.prepare("SELECT * FROM users WHERE id = :id");
    query.bindValue(":id", user_id);
    if(!query.exec())
    {
        qDebug()<<"Get social media error"<<query.lastError();
        u.id=-1;
        return u;
    }
    if(query.next())
    {

        u.fullname=query.value("fullname").toString().toStdString();
        u.username=query.value("username").toString().toStdString();
        u.password_hash=query.value("password_hash").toString().toStdString();
        u.id=query.value("id").toInt();
        return u;
    }
    else
    {
        u.id=-1;
        return u;
    }
}
vector<Social> SqliteStorage::getAllUserSocial(const int const_page, int user_id, string text)
{
    vector<Social> socials;
    int page_size=3;
    QSqlQuery query;
    query.prepare("SELECT * FROM social_media WHERE user_id = :user_id AND name LIKE ('%' || :search_text || '%') LIMIT :page_size OFFSET :skipped_items ");
    query.bindValue(":user_id", user_id);
    query.bindValue(":page_size", page_size);
    query.bindValue(":search_text", QString::fromStdString(text));
    int skipped_items = (const_page - 1) * page_size;
    query.bindValue(":skipped_items", skipped_items);
    if(!query.exec())
    {
        qDebug()<<"Get pagination error"<<query.lastError();

    }
    while(query.next())
    {
        Social s=getMediafromQuery(query);
        socials.push_back(s);
    }
    return socials;
}
int SqliteStorage::items_total(int user_id, string text)
{
    QSqlQuery query;
    query.prepare("SELECT COUNT(*) FROM social_media WHERE user_id=:user_id AND name LIKE ('%' || :search_text || '%')");
    query.bindValue(":user_id", user_id);
    query.bindValue(":search_text", QString::fromStdString(text));
    if(!query.exec())
    {
        qDebug()<<"Get count error"<<query.lastError();
        return -1;
    }
    else if(query.next())
    {
        return query.value(0).toInt();
    }
    else return -1;
}

vector<Groups> SqliteStorage::getAllGroups(const int const_page, string text)
{
    int page_size=4;
    QSqlQuery query;
    query.prepare("SELECT * FROM groups WHERE name LIKE ('%' || :search_text || '%') LIMIT :page_size OFFSET :skipped_items ");
    vector<Groups> gr;
    query.bindValue(":page_size", page_size);
    query.bindValue(":search_text", QString::fromStdString(text));
    int skipped_items = (const_page - 1) * page_size;
    query.bindValue(":skipped_items", skipped_items);
    if(!query.exec())
    {
        qDebug()<<"Get pagination group error"<<query.lastError();

    }
    while (query.next())
    {
        Groups g=getGroupfromQuery(query);
        gr.push_back(g);
    }
    return gr;
}

int SqliteStorage::groups_total(string text)
{
    QSqlQuery query;
    int res=0;
    query.prepare("SELECT COUNT(*) FROM groups WHERE name LIKE ('%' || :search_text || '%')");
    query.bindValue(":search_text", QString::fromStdString(text));
    if(!query.exec())
    {
        qDebug()<<"Get count group error"<<query.lastError();
        return -1;
    }
    else if(query.next())
    {
        res=query.value(0).toInt();
    }
    return res;
}

vector<Groups> SqliteStorage::getSocialGroups(vector<Groups> gr, int const_page, string text)
{
    int page_size=2;
    QSqlQuery query;
    int count=0, count2=0;
    int skipped_items = (const_page - 1) * page_size;
    vector<Groups> group;
    for(auto it=gr.begin(); it<gr.end(); it++)
    {
        query.prepare("SELECT * FROM groups WHERE id=:id AND name LIKE ('%' || :search_text || '%')");
        query.bindValue(":id", it->id);
        query.bindValue(":search_text", QString::fromStdString(text));

        if(!query.exec())
        {
            qDebug()<<"Get pagination group error"<<query.lastError();

        }
        if(query.next() && count>=skipped_items && count2<page_size)
        {
            Groups g=getGroupfromQuery(query);
            group.push_back(g);
            count2++;
        }
        count++;
    }
    return group;

}
int SqliteStorage::groups_total2(int social_id, string text)
{
    int count=0;
    QSqlQuery query;
    query.prepare("SELECT * FROM links WHERE social_id = :social_id");
    query.bindValue(":social_id", social_id);
    if(!query.exec())
    {
        qDebug()<<"Get link error"<<query.lastError();
    }
    while(query.next())
    {
        int group_id = query.value("group_id").toInt();
        QSqlQuery query2;
        query2.prepare("SELECT COUNT(*) FROM groups WHERE id=:id AND name LIKE ('%' || :search_text || '%')");
        query2.bindValue(":id",group_id );
        query2.bindValue(":search_text", QString::fromStdString(text));
        if(!query2.exec())
        {
            qDebug()<<"Get count group error"<<query2.lastError();
            return -1;
        }
        if(query2.next() && query2.value(0).toInt()!=0)
        {
            count++;
        }
    }
    return count;
}
bool SqliteStorage::registr(Users u)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM users WHERE username=:username OR password_hash=:password_hash");
    query.bindValue(":username", QString::fromStdString(u.username));
    query.bindValue(":password_hash", QString::fromStdString(u.password_hash));
    if(!query.exec())
    {
        qDebug()<<"Get username error"<<query.lastError();
    }
    if (query.next())
    {
        return false;
    }
    else
        return true;
}
