#include "remoteui.h"
RemoteUi::RemoteUi(Storage * storage):storage_(storage){}

void RemoteUi::run()
{
    rpc::server srv(8080);
    srv.suppress_exceptions(true);
    srv.bind("insertMedia",[&](const Social &media, int user_id)
    {
        return storage_->insertMedia(media, user_id);
    });
    srv.bind("getAllMedia",[&](void)
    {
        return storage_->getAllMedia();
    });
    srv.bind("AllUserMedia",[&](int user_id)
    {
        return storage_->AllUserMedia(user_id);
    });
    srv.bind("updateMedia",[&](const Social &media)
    {
        return storage_->updateMedia(media);
    });
    srv.bind("removeMedia",[&](int id)
    {
        return storage_->removeMedia(id);
    });
    srv.bind("allGroups",[&]()
    {
        return storage_->allGroups();
    });
    srv.bind("getGroupById",[&](int id)
    {
        return storage_->getGroupById(id);
    });
    srv.bind("updateGroup",[&](const Groups &group)
    {
        return storage_->updateGroup(group);
    });
    srv.bind("removeGroup",[&](int id)
    {
        return storage_->removeGroup(id);
    });
    srv.bind("insertGroup",[&](const Groups &group)
    {
        return storage_->insertGroup(group);
    });
    // users
    srv.bind("getUserAuth",[&]( const string & username, const string & password)
    {
        return storage_->getUserAuth(username,password);
    });
    srv.bind("getAllUserSocial",[&](const int const_page, int user_id, string text)
    {
        return storage_->getAllUserSocial(const_page, user_id, text);
    });
    srv.bind("items_total",[&](int user_id, string text)
    {
        return storage_->items_total( user_id,text);
    });
    srv.bind("addUserid",[&](int user_id, int gid)
    {
        return storage_->addUserid( user_id,gid);
    });
    srv.bind("insertUser",[&](const Users &user)
    {
        return storage_->insertUser( user);
    });
    srv.bind("getUserbyid",[&](const int user_id)
    {
        return storage_->getUserbyid( user_id);
    });
    // links
    srv.bind("getAllSocialGroups",[&](int social_id)
    {
        return storage_->getAllSocialGroups( social_id);
    });
    srv.bind("insertSocialGroups",[&](int social_id, int group_id)
    {
        return storage_->insertSocialGroups( social_id, group_id);
    });
    srv.bind("removeSocialGroups",[&](int social_id, int group_id)
    {
        return storage_->removeSocialGroups( social_id, group_id);
    });
    srv.bind("getAllGroups",[&](const int const_page,string text)
    {
        return storage_->getAllGroups( const_page, text);
    });
    srv.bind("getSocialGroups",[&](vector<Groups> gr, int const_page, string text)
    {
        return storage_->getSocialGroups( gr, const_page, text);
    });
    srv.bind("groups_total2",[&](int social_id, string text)
    {
        return storage_->groups_total2( social_id, text);
    });
    srv.bind("groups_total",[&]( string text)
    {
        return storage_->groups_total(  text);
    });
    srv.bind("registr",[&]( Users u)
    {
        return storage_->registr(  u);
    });
    srv.async_run(5);
    string str;
    while (str != "stop")
    {
        getline(cin, str);
    }
    exit(0);
}
