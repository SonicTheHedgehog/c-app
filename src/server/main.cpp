#include <QCoreApplication>
#include <rpc/server.h>
#include "storage.h"
#include "sqlite_storage.h"
#include "remoteui.h"
int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    Storage * tmp = new SqliteStorage("/home/sonya/progbase3/data/sql");
    tmp->open();
    RemoteUi server(tmp);
    server.run();
    tmp->close();
    return a.exec();
}
